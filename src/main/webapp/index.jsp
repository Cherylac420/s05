<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>Job Finder</title>
	<style>
		div{
			margin-top: 5px;
			margin-bottom: 5px;
		}
	</style>
</head>
<body>
	<h1>Welcome to Servlet Job Finder!</h1>
	
	<form action="register" method="post">
		<div>
			<label for="fname">First Name</label>
			<input type="text" name="fname" required>
		</div>
		
		<div>
			<label for="lname">Last Name</label>
			<input type="text" name="lname" required>
		</div>
		
		<div>
			<label for="phone">Phone</label>
			<input type="tel" name="phone" required>
		</div>
		
		<div>
			<label for="email">Email</label>
			<input type="email" name="email" required>
		</div>
		
		
		<fieldset>
			<legend>How did you discover the app?</legend>
			
			<input type="radio" id="firends" name="discovery" value="Friends" required>
			<label for="friends">Friends</label>
			<br>
			
			<input type="radio" id="socialMedia" name="discovery" value="Social Media" required>
			<label for="socialMedia">Social Media</label>
			<br>
			
			<input type="radio" id="others" name="discovery" value="others" required>
			<label for="others">Other</label>
			<br>
		</fieldset>
		
			
			<div>
				<label for="dob">Date of birth</label>
				<input type="date" name="dob" required>
			</div>
			
			<div>
				<label for="regType">Are you an Employer or Applicant?</label>
				<select id="type" name="regType">
					<option value="" selected="selected">Select one</option>
					<option value="Applicant">Applicant</option>
					<option value="Employer">Employer</option>
				</select>
			</div>
			
			<div>
				<label for="profile">Profile Description</label>
				<textarea name="profile" maxlength="500"></textarea>
			</div>
			
			<button>Register</button>
			
	</form>
	
	
	
</body>
</html>