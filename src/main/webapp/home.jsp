<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>Home</title>
</head>
<body>
	<%
		String message = "";
		String type = session.getAttribute("regType").toString();
		String name = session.getAttribute("fullname").toString();
		if(type.equals("Applicant")){
			message = "Welcome aplicant. You may now start looking for your career opportunity.";
		}
		else{
			message = "Welcome employer. You may now start browsing applicant profiles.";
		}
	%>
	
	<h1>Welcome <%= name%>!</h1>
	<p><%= message%></p>
</body>
</html>